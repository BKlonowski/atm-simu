#include "CardReader.hpp"



// Definition of the card counter
int CardReaderC::numberOfInsertedCards = 0;


CardReaderC::CardReaderC()
{
    InitializeNewCardReader();
}


CardReaderC::CardReaderC( const CardReaderC& card )
{
    InitializeNewCardReader();
}


CardReaderC::~CardReaderC()
{
    numberOfInsertedCards--;
}


const int CardReaderC::GiveCardSlotNumber( void ) const
{
    return thisCardReaderNumber;
}


void CardReaderC::OpenAccount( const std::string& id, const std::string& pin )
{
    if( !DatabaseC::IsUserBlocked( id ) )
        account = std::make_unique<AccountC>( id, pin );
    else
        throw std::exception( "ERROR: User is currently blocked!" );
}


const std::string& CardReaderC::DisplayUserID( void ) const
{
    return account->userID;
}


const int& CardReaderC::DisplayUserAvailableCash( void ) const
{
    return account->cashAvailable;
}


void CardReaderC::InitializeNewCardReader( void )
{
    if( ++numberOfInsertedCards > MAXIMUM_CARD_SLOT_NUMBER )
    {
        throw std::overflow_error( "ERROR:  No more free card slots available!" );
    }
    else
    {
        thisCardReaderNumber = numberOfInsertedCards;
    }
}


void CardReaderC::BlockCard( void )
{
    account->blocked = true;
}


void CardReaderC::RequestWithdrawal( const int& requestedCash )
{
    account->DebitAccount( requestedCash );
}
