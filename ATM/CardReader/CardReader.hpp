#pragma once

#include "../../Bank/Account/Account.hpp"

#include <string>
#include <memory>


// This class represents the card slot and the uC of card reader
// It is responsible for reading the card and communicate its status with other modules
class CardReaderC
{
public:
    // Default constructor called when new card is inserted
    CardReaderC();

    // Copy constructor used to add another card or account of the user
    CardReaderC( const CardReaderC& card );

    // The destructor will have to dispence the inserted card
    ~CardReaderC();

    // Gives the number of the readers instance
    const int GiveCardSlotNumber( void ) const;

    // Connects to the specified account if possible - if account exists and data are correct
    void OpenAccount( const std::string& id, const std::string& pin );

    // Blocks the card by changing its status in the account information
    void BlockCard( void );

    void RequestWithdrawal( const int& requestedCash );

    const std::string& DisplayUserID( void ) const;
    const int& DisplayUserAvailableCash( void ) const;


    // Counts the current amount of card inserted into the ATM
    // NOTE: this number should not exceed 2
    static int numberOfInsertedCards;



private:
    // Initializes the new instance of the card reader entity
    void InitializeNewCardReader( void );



    std::unique_ptr<AccountC> account;

    // The number of the current instance of the card reader
    int thisCardReaderNumber;

    constexpr static int MAXIMUM_CARD_SLOT_NUMBER = 2;
};
