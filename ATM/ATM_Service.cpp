#include "ATM_Service.hpp"


ATM_ServiceC::ATM_ServiceC()
{
    DatabaseC::InitializeAndConnect();
}


void ATM_ServiceC::StartButtonProcedure( const std::string& id, const std::string& pin )
{
    card = std::make_unique<CardReaderC>();
    card->OpenAccount( id, pin );
}


void ATM_ServiceC::CreateUserProcedure( const std::string& id, const std::string& pin )
{
    if( id.empty() || pin.empty() )
        throw std::exception( "ERROR: User data empty!" );

    Bank_ServiceC bank;
    if( !bank.IsUserCreated( id ) )
        bank.CreateNewUser( id, pin );
    else
        throw std::exception( "ERROR: User already exists!" );
}


void ATM_ServiceC::PrintButtonProcedure( void )
{
    dispencer = std::make_unique<DispencerC>();
    dispencer->ParseUserID( card->DisplayUserID() );
    dispencer->ParseAvailableCash( card->DisplayUserAvailableCash() );

    dispencer->PrintWithdrawnTicket();
}


void ATM_ServiceC::OKButtonProcedure( const int& requestedCash )
{
    card->RequestWithdrawal( requestedCash );
}


void ATM_ServiceC::FinishButtonProcedure( void )
{
    // At the end of the OK procedure drop the card out of the ATM
    card.release();
}
