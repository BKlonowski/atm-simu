#include "Dispencer.hpp"
#include <Windows.h>
#include <WinBase.h>


void DispencerC::ParseWithdrawnCash( const int& withdrawnCash )
{
    this->withdrawnCash = withdrawnCash;
}


void DispencerC::ParseUserID( const std::string& userID )
{
    this->userID = userID;
}


void DispencerC::ParseAvailableCash( const int& availableCash )
{
    this->availableCash = availableCash;
}


void DispencerC::PrintWithdrawnTicket( void )
{
    std::fstream withdrawnTicket;
    withdrawnTicket.open( withdrawnTicketFileName, std::ios::out );

    if( withdrawnTicket.is_open() )
    {
        PrintTicketHeader( withdrawnTicket );

        // Put the main information about the user and operation
        withdrawnTicket.write( "| User ID: ", strlen( "| User ID: " ) );
        withdrawnTicket.write( userID.c_str(), userID.size() );
        withdrawnTicket.write( "\n", strlen( "\n" ) );
        withdrawnTicket.write( "|\n| ", strlen( "|\n| " ) );
        withdrawnTicket.write( std::to_string( withdrawnCash ).c_str(), std::to_string( withdrawnCash ).size() );
        withdrawnTicket.write( "\n------------------\n", strlen( "\n------------------\n" ) );
    }
}


void DispencerC::PrintDepositTicket( void )
{
}


void DispencerC::PrintTicketHeader( std::fstream& ticket )
{
    const std::string ticketHeader = "------------------\n| ATM-Simulator - withdrawn ticket.\n|\n| Date and time:\n";

    // Print the header with time and data and separate it
    ticket.write( ticketHeader.c_str(), ticketHeader.size() );
    ticket.write( GetSystemDateString().c_str(), GetSystemDateString().size() );
    ticket.write( GetSystemTimeString().c_str(), GetSystemTimeString().size() );
    ticket.write( "|\n", strlen( "|\n" ) );
}


std::string DispencerC::GetSystemDateString( void )
{
    SYSTEMTIME systemTime = { 0 };
    GetSystemTime( &systemTime );

    return "| " + std::to_string( systemTime.wDay ) + "."
        + std::to_string( systemTime.wMonth ) + "."
        + std::to_string( systemTime.wYear ) + "\n";
}


std::string DispencerC::GetSystemTimeString( void )
{
    SYSTEMTIME systemTime = { 0 };
    GetSystemTime( &systemTime );

    return "| " + std::to_string( systemTime.wHour ) +":"
        + std::to_string( systemTime.wMinute ) + "\n";
}
