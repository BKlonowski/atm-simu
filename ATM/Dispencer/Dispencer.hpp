#pragma once

#include <string>
#include <fstream>


class DispencerC
{
public:
    // Methdos used to gather the data to be printed
    void ParseWithdrawnCash( const int& withdrawnCash );

    void ParseUserID( const std::string& userID );

    void ParseAvailableCash( const int& availableCash );
     

    // Methods used to print specific tickets
    void PrintWithdrawnTicket( void );

    void PrintDepositTicket( void );


private:
    void PrintTicketHeader( std::fstream& ticket );

    std::string GetSystemDateString( void );
    std::string GetSystemTimeString( void );

    constexpr static auto withdrawnTicketFileName = "withdrawn.txt";

    // Information about performed operation
    int withdrawnCash;
    std::string userID;
    int availableCash;
};
