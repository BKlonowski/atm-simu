#pragma once

#include "CardReader\CardReader.hpp"
#include "Dispencer\Dispencer.hpp"


class ATM_ServiceC
{
public:
    ATM_ServiceC();

    void StartButtonProcedure( const std::string& id, const std::string& pin );

    void CreateUserProcedure( const std::string& id, const std::string& pin );

    void PrintButtonProcedure( void );

    void OKButtonProcedure( const int& requestedCash );

    void FinishButtonProcedure( void );

private:
    std::unique_ptr<CardReaderC> card;

    std::unique_ptr<DispencerC> dispencer;

};
