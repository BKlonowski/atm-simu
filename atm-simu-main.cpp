#ifndef UNICODE
#define UNICODE
#endif

#include "AppWindow\AppWindow.hpp"



int WINAPI wWinMain( HINSTANCE hInstance, HINSTANCE, PWSTR pCmdLine, int nCmdShow )
{
    std::unique_ptr<WindowC> MainWindow = std::make_unique<WindowC>();

    WNDCLASS wc = {};

    wc.lpfnWndProc = MainWindow->WindowProc;
    wc.hInstance = hInstance;
    wc.lpszClassName = MainWindow->CLASS_NAME;

    RegisterClass( &wc );

    // Create the window.
    MainWindow->InitWindowHandler( hInstance );

    if( MainWindow->hWindow == nullptr )
    {
        return 0;
    }
    MainWindow->InitializeMainControlls( hInstance );
    ShowWindow( MainWindow->hWindow, nCmdShow );


    // Run the message loop.
    MSG msg = {};
    while( GetMessage( &msg, NULL, 0, 0 ) )
    {
        TranslateMessage( &msg );
        DispatchMessage( &msg );
    }

    return 0;
}
