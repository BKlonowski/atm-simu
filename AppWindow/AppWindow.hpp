#pragma once

#define UNICODE


#include <Windows.h>
#include <array>

#include "..\ATM\ATM_Service.hpp"



class WindowC
{
public:
    // Default constructor of the main window
    WindowC();

    // Initialize the public main window handler with the application GUI properties
    void InitWindowHandler( HINSTANCE& hInstance );

    // Public WindowProc callback - initialize the local internal object which handles the callback
    static LRESULT CALLBACK WindowProc( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam );

    // Fills the protected array with the proper controlls according to the MainControlls_t
    void InitializeMainControlls( HINSTANCE& hInstance );

    // Enables the controlls related to the withdrawal operations
    void EnableWithdrawalControlls( void ) const;

    // Disables the controlls related to the withdrawal operations
    void DisableWithdrawalControlls( void ) const;

    // Getters for the private user authentication data such as PIN and ID
    const std::string& GetUserID( void ) const;
    const std::string& GetUSerPIN( void ) const;


    // Window class name used while the registration
    constexpr static wchar_t CLASS_NAME[] = L"MainWindowClass";

    // Public handler of the main window
    HWND hWindow;




protected:
    // Set of controlls in the main window which will be accessed in the callback
    typedef enum
    {
        // General operation controlls
        Start_Button,
        UserID_TextFrame,
        CardPin_TextFrame,
        Create_Button,
        Finish_Button,

        // Withdrawal operation controlls
        CashAmount_TextFrame,
        OK_Button,
        Print_Button,

        MaxControllNumber
    } MainControlls_t;


    // The rest of controlls in the window
    std::array<HWND, MaxControllNumber> controlls;


private:
    // The real callback procedure masked by the public WindowProc method
    // This one is called after the proper and correct object of main window has been created
    LRESULT CALLBACK InternalWindowProc( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam );

    // the methods to pull the texts from specific text frames
    void ReadFrame( const MainControlls_t textFrame );

    // Clears all the data of the user - removes the card, resets the text frames, etc
    void ClearUserAuthData( void );


    // Maximum length of the user data (pin and ID) = 4 characters + 1 null character
    constexpr static int MAX_USER_DATA_TEXT_LENGTH = 5;

    // The texts pulled from the dedicated text fields
    std::string userPIN;
    std::string userID;
    std::string requestedCash;

    // Holds the ATM functionality in the main window
    ATM_ServiceC atm;

    // The handler of two section frames in the main window - withdrawal and deposit options
    HWND withdrawalFrame;
    HWND depositFrame;
};
