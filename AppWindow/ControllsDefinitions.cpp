#include "AppWindow.hpp"
#include "WindowConfiguration.hpp"



void WindowC::InitializeMainControlls( HINSTANCE& hInstance )
{
    withdrawalFrame = CreateWindowEx( WS_THICKFRAME, L"BUTTON", L"Withdrawal",
        WS_CHILD | WS_VISIBLE | BS_GROUPBOX,
        WITHDRAWAL_FRAME_POS_X, WITHDRAWAL_FRAME_POS_Y, WITHDRAWAL_FRAME_SIZE_X, WITHDRAWAL_FRAME_SIZE_Y,
        hWindow, nullptr, hInstance, nullptr );

    controlls[ Start_Button ] = CreateWindowEx( 0, L"BUTTON", L"START",
        WS_CHILD | WS_VISIBLE,
        START_BUTTON_POS_X, START_BUTTON_POS_Y, START_BUTTON_SIZE_X, START_BUTTON_SIZE_Y,
        hWindow, reinterpret_cast<HMENU>(Start_Button), hInstance, nullptr );

    controlls[ Start_Button ] = CreateWindowEx( 0, L"BUTTON", L"CREATE",
        WS_CHILD | WS_VISIBLE,
        CREATE_BUTTON_POS_X, CREATE_BUTTON_POS_Y, CREATE_BUTTON_SIZE_X, CREATE_BUTTON_SIZE_Y,
        hWindow, reinterpret_cast<HMENU>(Create_Button), hInstance, nullptr );

    controlls[ UserID_TextFrame ] = CreateWindowEx( WS_EX_CLIENTEDGE, L"EDIT", L"",
        WS_CHILD | WS_VISIBLE | ES_NUMBER | ES_CENTER,
        USER_ID_FRAME_POS_X, USER_ID_FRAME_POS_Y, USER_ID_FRAME_SIZE_X, USER_ID_FRAME_SIZE_Y,
        hWindow, reinterpret_cast<HMENU>(UserID_TextFrame), hInstance, nullptr );

    controlls[ CardPin_TextFrame ] = CreateWindowEx( WS_EX_CLIENTEDGE, L"EDIT", L"",
        WS_CHILD | WS_VISIBLE | BS_TEXT | ES_PASSWORD | ES_NUMBER | ES_CENTER,
        CARD_PIN_FRAME_POS_X, CARD_PIN_FRAME_POS_Y, CARD_PIN_FRAME_SIZE_X, CARD_PIN_FRAME_SIZE_Y,
        hWindow, reinterpret_cast<HMENU>(CardPin_TextFrame), hInstance, nullptr );

    controlls[ CashAmount_TextFrame ] = CreateWindowEx( WS_EX_CLIENTEDGE, L"EDIT", L"",
        WS_CHILD | WS_VISIBLE | BS_TEXT | ES_NUMBER | ES_CENTER | WS_DISABLED,
        CASH_AMOUNT_FRAME_POS_X, CASH_AMOUNT_FRAME_POS_Y, CASH_AMOUNT_FRAME_SIZE_X, CASH_AMOUNT_FRAME_SIZE_Y,
        hWindow, reinterpret_cast<HMENU>(CashAmount_TextFrame), hInstance, nullptr );

    controlls[ OK_Button ] = CreateWindowEx( 0, L"BUTTON", L"OK",
        WS_CHILD | WS_VISIBLE | WS_DISABLED,
        OK_BUTTON_POS_X, OK_BUTTON_POS_Y, OK_BUTTON_SIZE_X, OK_BUTTON_SIZE_Y,
        hWindow, reinterpret_cast<HMENU>(OK_Button), hInstance, nullptr );

    controlls[ Print_Button ] = CreateWindowEx( 0, L"BUTTON", L"PRINT",
        WS_CHILD | WS_VISIBLE | WS_DISABLED,
        PRINT_BUTTON_POS_X, PRINT_BUTTON_POS_Y, PRINT_BUTTON_SIZE_X, PRINT_BUTTON_SIZE_Y,
        hWindow, reinterpret_cast<HMENU>(Print_Button), hInstance, nullptr );

    controlls[ Finish_Button ] = CreateWindowEx( 0, L"BUTTON", L"FINISH",
        WS_CHILD | WS_VISIBLE,
        FINISH_BUTTON_POS_X, FINISH_BUTTON_POS_Y, FINISH_BUTTON_SIZE_X, FINISH_BUTTON_SIZE_Y,
        hWindow, reinterpret_cast<HMENU>(Finish_Button), hInstance, nullptr );

    // Create the labels for all the text fields and boxes
    CreateWindow( L"STATIC", L"User ID:",
        WS_CHILD | WS_VISIBLE | WS_EX_TRANSPARENT,
        USER_ID_LABEL_POS_X, USER_ID_LABEL_POS_Y, USER_ID_LABEL_SIZE_X, USER_ID_LABEL_SIZE_Y,
        hWindow, nullptr, hInstance, nullptr );
    CreateWindow( L"STATIC", L"PIN:",
        WS_CHILD | WS_VISIBLE,
        CARD_PIN_LABEL_POS_X, CARD_PIN_LABEL_POS_Y, CARD_PIN_LABEL_SIZE_X, CARD_PIN_LABEL_SIZE_Y,
        hWindow, nullptr, hInstance, nullptr );
    CreateWindow( L"STATIC", L"Cash amount:",
        WS_CHILD | WS_VISIBLE,
        CASH_AMOUNT_LABEL_POS_X, CASH_AMOUNT_LABEL_POS_Y, CASH_AMOUNT_LABEL_SIZE_X, CASH_AMOUNT_LABEL_SIZE_Y,
        hWindow, nullptr, hInstance, nullptr );
}
