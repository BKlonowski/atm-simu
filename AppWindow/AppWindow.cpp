#include "AppWindow.hpp"
#include "WindowConfiguration.hpp"




WindowC::WindowC() : userID(""), userPIN(""), requestedCash("")
{
}


void WindowC::InitWindowHandler( HINSTANCE& hInstance )
{
    hWindow = ::CreateWindowExW( 0,                                             // Optional window styles.
        CLASS_NAME,                                                             // Window class
        L"ATM",                                                                 // Window text
        WS_OVERLAPPEDWINDOW,                                                    // Window style
        CW_USEDEFAULT, CW_USEDEFAULT, MAIN_WINDOW_SIZE_X, MAIN_WINDOW_SIZE_Y,   // Size and position
        NULL,                                                                   // Parent window
        NULL,                                                                   // Menu
        hInstance,                                                              // Instance handle
        this                                                                    // Additional application data
    );
    SetWindowLongPtr( hWindow, GWLP_USERDATA, (long)this );
}


LRESULT CALLBACK WindowC::WindowProc( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    WindowC* internalWindowObject = nullptr;
    if( uMsg == WM_NCCREATE )
    {
        ::SetWindowLong( hwnd, -21, long( (LPCREATESTRUCT( lParam ))->lpCreateParams ) );
    }

    internalWindowObject = reinterpret_cast<WindowC*>(::GetWindowLong( hwnd, -21 ));

    if( internalWindowObject != nullptr )
        return internalWindowObject->InternalWindowProc( hwnd, uMsg, wParam, lParam );
    else
        return ::DefWindowProc( hwnd, uMsg, wParam, lParam );
}


LRESULT CALLBACK WindowC::InternalWindowProc( HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
    switch( uMsg )
    {
    case WM_DESTROY:
        DatabaseC::Disconnect();
        PostQuitMessage( 0 );
        return 0;

    case WM_PAINT:
    {
        PAINTSTRUCT ps;

        FillRect( BeginPaint( hwnd, &ps ), &ps.rcPaint, (HBRUSH)(COLOR_WINDOW) );

        EndPaint( hwnd, &ps );
    }
    return 0;

    case WM_COMMAND:
        try
        {
            switch( wParam )
            {

            case Start_Button:
                ReadFrame( UserID_TextFrame );
                ReadFrame( CardPin_TextFrame );
                atm.StartButtonProcedure( userID, userPIN );
                EnableWithdrawalControlls();
                break;

            case Create_Button:
                ReadFrame( UserID_TextFrame );
                ReadFrame( CardPin_TextFrame );
                atm.CreateUserProcedure( userID, userPIN );
                MessageBoxA( hwnd, "User created successfully!", "OK!", 0 );
                ClearUserAuthData();
                break;

            case OK_Button:
                ReadFrame( CashAmount_TextFrame );
                atm.OKButtonProcedure( std::atoi( requestedCash.c_str() ) );
                MessageBoxA( hwnd, "Withdrawal operation completed!", "OK!", 0 );
                break;

            case Print_Button:
                atm.PrintButtonProcedure();
                break;

            case Finish_Button:
                atm.FinishButtonProcedure();
                ClearUserAuthData();
                DisableWithdrawalControlls();
                MessageBoxA( hwnd, "See you again!", "Logged out!", 0 );
                break;

            default:
                break;
            }
            return 0;
        }
        catch( std::exception& e )
        {
            MessageBoxA( hwnd, e.what(), "ERROR!", 0 );
            ClearUserAuthData();
        }
    }
    return DefWindowProc( hwnd, uMsg, wParam, lParam );
}


void WindowC::ReadFrame( const MainControlls_t textFrame )
{
    wchar_t* tmpTextBuffer = new wchar_t[ MAX_USER_DATA_TEXT_LENGTH ];
    memset( tmpTextBuffer, '\0', MAX_USER_DATA_TEXT_LENGTH );
    GetWindowText( controlls.at( textFrame ), tmpTextBuffer, MAX_USER_DATA_TEXT_LENGTH );

    switch( textFrame )
    {
    case MainControlls_t::UserID_TextFrame:
        for( int i = 0; i < MAX_USER_DATA_TEXT_LENGTH; ++i )
            userID.push_back( static_cast<char>(tmpTextBuffer[ i ]) );
        break;

    case MainControlls_t::CardPin_TextFrame:
        for( int i = 0; i < MAX_USER_DATA_TEXT_LENGTH; ++i )
            userPIN.push_back( static_cast<char>(tmpTextBuffer[ i ]) );
        break;

    case MainControlls_t::CashAmount_TextFrame:
        for( int i = 0; i < MAX_USER_DATA_TEXT_LENGTH; ++i )
            requestedCash.push_back( static_cast<char>(tmpTextBuffer[ i ]) );
        break;

    default:
        break;
    }

    delete[] tmpTextBuffer;
}


void WindowC::EnableWithdrawalControlls( void ) const
{
    EnableWindow( controlls.at( CashAmount_TextFrame ), true );
    EnableWindow( controlls.at( OK_Button ), true );
    EnableWindow( controlls.at( Print_Button ), true );
}


void WindowC::DisableWithdrawalControlls( void ) const
{
    EnableWindow( controlls.at( CashAmount_TextFrame ), false );
    EnableWindow( controlls.at( OK_Button ), false );
    EnableWindow( controlls.at( Print_Button ), false );
}


const std::string& WindowC::GetUserID( void ) const
{
    return userID;
}


const std::string& WindowC::GetUSerPIN( void ) const
{
    return userPIN;
}

void WindowC::ClearUserAuthData( void )
{
    userID.clear();
    userPIN.clear();
    requestedCash.clear();

    SetWindowTextA( controlls.at( UserID_TextFrame ), "" );
    SetWindowTextA( controlls.at( CardPin_TextFrame ), "" );
    SetWindowTextA( controlls.at( CashAmount_TextFrame ), "" );
}
