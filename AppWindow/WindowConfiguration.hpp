#pragma once

// The size and positions of the application window and all its components

// Main window
constexpr int MAIN_WINDOW_SIZE_X = 660;
constexpr int MAIN_WINDOW_SIZE_Y = 360;


// General operation controlls
constexpr int START_BUTTON_SIZE_X = 60;
constexpr int START_BUTTON_SIZE_Y = 30;
constexpr int START_BUTTON_POS_X = 50;
constexpr int START_BUTTON_POS_Y = 200;

constexpr int CREATE_BUTTON_SIZE_X = 60;
constexpr int CREATE_BUTTON_SIZE_Y = 30;
constexpr int CREATE_BUTTON_POS_X = 50;
constexpr int CREATE_BUTTON_POS_Y = 150;

constexpr int USER_ID_FRAME_SIZE_X = 120;
constexpr int USER_ID_FRAME_SIZE_Y = 30;
constexpr int USER_ID_FRAME_POS_X = 20;
constexpr int USER_ID_FRAME_POS_Y = 40;

constexpr int CARD_PIN_FRAME_SIZE_X = 120;
constexpr int CARD_PIN_FRAME_SIZE_Y = 30;
constexpr int CARD_PIN_FRAME_POS_X = 20;
constexpr int CARD_PIN_FRAME_POS_Y = 100;

constexpr int FINISH_BUTTON_SIZE_X = 60;
constexpr int FINISH_BUTTON_SIZE_Y = 30;
constexpr int FINISH_BUTTON_POS_X = 50;
constexpr int FINISH_BUTTON_POS_Y = 250;


// Withdrawal frame and its operation controlls
constexpr int WITHDRAWAL_FRAME_SIZE_X = 230;
constexpr int WITHDRAWAL_FRAME_SIZE_Y = 300;
constexpr int WITHDRAWAL_FRAME_POS_X = 150;
constexpr int WITHDRAWAL_FRAME_POS_Y = 5;

constexpr int CASH_AMOUNT_FRAME_SIZE_X = 200;
constexpr int CASH_AMOUNT_FRAME_SIZE_Y = 30;
constexpr int CASH_AMOUNT_FRAME_POS_X = 160;
constexpr int CASH_AMOUNT_FRAME_POS_Y = 70;

constexpr int OK_BUTTON_SIZE_X = 60;
constexpr int OK_BUTTON_SIZE_Y = 30;
constexpr int OK_BUTTON_POS_X = 170;
constexpr int OK_BUTTON_POS_Y = 250;

constexpr int PRINT_BUTTON_SIZE_X = 60;
constexpr int PRINT_BUTTON_SIZE_Y = 30;
constexpr int PRINT_BUTTON_POS_X = 250;
constexpr int PRINT_BUTTON_POS_Y = 250;


// Labels describing the text boxes
constexpr int USER_ID_LABEL_SIZE_X = 80;
constexpr int USER_ID_LABEL_SIZE_Y = 20;
constexpr int USER_ID_LABEL_POS_X = 20;
constexpr int USER_ID_LABEL_POS_Y = 20;

constexpr int CARD_PIN_LABEL_SIZE_X = 80;
constexpr int CARD_PIN_LABEL_SIZE_Y = 20;
constexpr int CARD_PIN_LABEL_POS_X = 20;
constexpr int CARD_PIN_LABEL_POS_Y = 80;

constexpr int CASH_AMOUNT_LABEL_SIZE_X = 100;
constexpr int CASH_AMOUNT_LABEL_SIZE_Y = 20;
constexpr int CASH_AMOUNT_LABEL_POS_X = 160;
constexpr int CASH_AMOUNT_LABEL_POS_Y = 50;

