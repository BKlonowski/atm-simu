#pragma once

#include "Account.hpp"


AccountC::AccountC( const std::string& userID, const std::string& userPIN ) : userID(userID), userPIN(userPIN)
{
    if( !AreUserDataValid( this->userID, this->userPIN ) )
        throw std::exception( "ERROR: User auth data invalid!" );

    this->blocked = false;
    this->cashAvailable = GetUserAvailableCash( this->userID );
}


void AccountC::DebitAccount( const int& cash )
{
    if( cash <= cashAvailable )
    {
        cashAvailable -= cash;
        UpdateAccountCash( userID, cashAvailable );
    }
    else
        throw std::exception( "ERROR: Not enough resources available!" );
}
