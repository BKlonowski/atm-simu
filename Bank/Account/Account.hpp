#pragma once

#include "..\Bank_Service.hpp"



class AccountC : public Bank_ServiceC
{
public:
    AccountC( const std::string& userID, const std::string& userPIN );

    void DebitAccount( const int& cash );


    // User ID can be visible after opening the account
    std::string userID;

    // Available cash owns by the account user
    int cashAvailable;

    // Indicates whether account is blocked or not
    bool blocked;


private:
    // Users PIN needs to be hidden, but assigned after opening the account
    std::string userPIN;
};
