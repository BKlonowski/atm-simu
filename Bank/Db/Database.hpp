#pragma once

#include "..\..\Dependencies\MySQL_lib\include\mysql.h"

#include <string>


class DatabaseC
{
public:
    virtual ~DatabaseC( void ) = 0;


    static void InitializeAndConnect( void )
    {
        mysql_init( &bankDB );
        if( nullptr == &bankDB )
        {
            throw std::exception( mysql_error( &bankDB ) );
        }

        if( nullptr == mysql_real_connect( &bankDB, "localhost", "root", "passwd", "atm_bank", 3936, nullptr, 0 ) )
        {
            throw std::exception( mysql_error( &bankDB ) );
        }
    };


    static const bool IsDatabaseConnected( void )
    {
        if( mysql_ping( &bankDB ) )
            return false;
        else
            return true;
    }


    static const int GetUserCash( const std::string& user_id )
    {
        return std::atoi( GetUserData( user_cash, user_id ).c_str() );
    }


    static const bool IsUserBlocked( const std::string& user_id )
    {
        const std::string& res = GetUserData( user_blocked, user_id );
        if( '1' == res[0] )
            return true;
        else
            return false;
    }


    static const std::string GetUserPassword( const std::string& user_id )
    {
        return GetUserData( user_pin, user_id );
    }


    static const bool IsUserInDatabase( const std::string& user_id )
    {
        try
        {
            return (user_id == GetUserData( UserData_t::user_id, user_id )) ? false : true;
        }
        catch( ... )
        {
            return false;
        }
    }


    static void UpdateUserCash( const std::string& user_id, const int& newCash )
    {
        std::string newQuery( "UPDATE `bank_accounts` SET cash = " + std::to_string( newCash ) + " WHERE user_id = " + user_id );
        SendQuery( newQuery );
    }


    static void Disconnect( void )
    {
        mysql_close( &bankDB );
    }


    static void SendQuery( const std::string& query )
    {
        mysql_query( &bankDB, query.c_str() );
    }


private:

    // these enumerated values represents the columns of the bank_accounts table
    typedef enum
    {
        user_id,
        user_pin,
        user_blocked,
        user_cash
    } UserData_t;


    static std::string GetUserData( const UserData_t userData, const std::string& user_id )
    {
        SendQuery( "SELECT * FROM bank_accounts WHERE user_id = " + user_id );

        MYSQL_RES* result = mysql_store_result( &bankDB );
        if( nullptr == result )
        {
            throw std::exception( mysql_error( &bankDB ) );
        }

        MYSQL_ROW user_row = mysql_fetch_row( result );
        if( nullptr == user_row )
        {
            throw std::exception( "ERROR: Data empty!" );
        }

        // In case of having the user unblocked the db result string will be null
        if( nullptr == user_row[ userData ] )
            return "0";
        else
            return user_row[ userData ];
    }


private:
    static MYSQL bankDB;
};
