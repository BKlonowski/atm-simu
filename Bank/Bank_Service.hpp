#pragma once

#include <string>
#include "Db\Database.hpp"


class Bank_ServiceC
{
public:
    Bank_ServiceC( void );

    bool AreUserDataValid( const std::string& userID, const std::string& userPIN );

    // Connects to the MySQL database or throws the exception when failure occurs
    void ConnectToDatabase( void );

    // Creates new user in the bank database with the empty (zeroed) cash amount
    void CreateNewUser( const std::string& userID, const std::string& userPIN );

    // Checks whether the user exists in the database
    bool IsUserCreated( const std::string& userID );

    // Pulls the available cash from the db by the user ID
    int GetUserAvailableCash( const std::string& userID );

    virtual void UpdateAccountCash( const std::string& userID, const int& newCash );
};
