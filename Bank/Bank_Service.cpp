#include "Bank_Service.hpp"


MYSQL DatabaseC::bankDB = { 0 };

Bank_ServiceC::Bank_ServiceC( void )
{
    if( !DatabaseC::IsDatabaseConnected() )
    {
        ConnectToDatabase();
    }
}


bool Bank_ServiceC::AreUserDataValid( const std::string& userID, const std::string& userPIN )
{
    const std::string&& userPassword = std::move( DatabaseC::GetUserPassword( userID ) );

    if( DatabaseC::IsUserInDatabase( userID ) && !std::strncmp( userPIN.c_str(), userPassword.c_str(), userPassword.length() ) )
        return true;
    else
        return false;
}


void Bank_ServiceC::ConnectToDatabase( void )
{
    DatabaseC::InitializeAndConnect();
}


void Bank_ServiceC::CreateNewUser( const std::string& userID, const std::string& userPIN )
{
    std::string newQuery( "INSERT INTO bank_accounts (user_id,user_pin) VALUES(" );
    newQuery += userID.c_str();
    newQuery += ',';
    newQuery += userPIN.c_str();
    newQuery += ")";
    DatabaseC::SendQuery( newQuery );
}


bool Bank_ServiceC::IsUserCreated( const std::string& userID )
{
    return DatabaseC::IsUserInDatabase( userID );
}


int Bank_ServiceC::GetUserAvailableCash( const std::string& userID )
{
    return DatabaseC::GetUserCash( userID );
}


void Bank_ServiceC::UpdateAccountCash( const std::string& userID, const int& newCash )
{
    DatabaseC::UpdateUserCash( userID, newCash );
}
