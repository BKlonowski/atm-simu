# ATM Simulation application #
---

* [Description](#markdown-header-description)
* [Architecture](#markdown-header-architecture)
* [Usage](#markdown-header-architecture)



---
## **Description** ##

The following application is the simulator of the ATM.

It allows to perform the most common operations such as withdrawal or the deposition of the users cash.

To summary the ATM-Simu application features:

* Database with its table is created when first launching the application
* Each new user ID is stored as a new record in the table with empty account
* One authentication data insertion lets only one operation to be performed





---
## **Architecture** ##

**NOTE:** The work is currently in progress - it means that there is no release version yet and the documentation of the project may not be complete...

![Sequence](Documentation/Images/sequenceDiagram.png)

The architecture of the whole system is presented in the diagram below:
![Architecture](Documentation/Images/SystemArchitecture-InProgress.png)


---
## **Usage** ##

After launching the application the following window should appear:
![WindowOverlook](Documentation/Images/CurrentWindowLook.PNG)

To continue with the use scenario enter the user ID (sequence of your personal account digits) and enter the PIN (Personal Identification Number).

After these steps the *START* button can be clicked, which will enable both operation frames (*Withdrawal* and *Deposit*).
Please note, that this will be enabled only if correct user authentication data were provided!
